package MediatorDesignPattern;
public class Client {
	public static void main(String[] args) {
		ChatMediator mediator = new ChatMediator();
		
		//creating users
		BasicUser basicUser = new BasicUser();
		basicUser.setChatMediator(mediator);
		basicUser.setName("ABCD");
		
		PremiumUser premiumUser = new PremiumUser();
		premiumUser.setChatMediator(mediator);
		premiumUser.setName("LMNO");
		
		BasicUser sourceUser = new BasicUser();
		sourceUser.setChatMediator(mediator);
		sourceUser.setName("WXYZ");
		
		
		//adding users
		mediator.AddUser(basicUser);
		mediator.AddUser(premiumUser);
		
		//sending broadcast message
		sourceUser.SendMessage();
	}
}
