package PracticeCheck;





import java.util.ArrayList;
import java.util.List;



public class Main {

public static void main(String[] args) {

INotificationObserver Steve=new SteveObserver();
INotificationObserver John=new JohnObserver();
INotificationService notificationService=new NotificationService();
System.out.println("Subscribers are:");
notificationService.addSubscriber(John);
notificationService.notifySubscriber();
System.out.println();
System.out.println("Subscribers are:");
notificationService.addSubscriber(Steve);
notificationService.notifySubscriber();
System.out.println();
System.out.println("Subscribers left after removing John are:");
notificationService.removeSubscriber(John);
notificationService.notifySubscriber();
}

}
 interface INotificationObserver {
public void onServerDown();
public String getName();
}
class SteveObserver implements INotificationObserver {

private String name;
public SteveObserver(){
name="Steve";
}
@Override
public void onServerDown() {
System.out.println(name+": Notification has been received");
}
public String getName() {
return name;
}

}
 class JohnObserver implements INotificationObserver {

private String name;
public JohnObserver() {
name="John";
}
@Override
public void onServerDown() {

System.out.println(name+": Notification has been received");
}
public String getName() {
return name;
}

}
 interface INotificationService {

public void addSubscriber(INotificationObserver o);
public void removeSubscriber(INotificationObserver o);
public void notifySubscriber();
}
 class NotificationService implements INotificationService {

	 private List<INotificationObserver> observers=new ArrayList<>();
	 @Override
	 public void addSubscriber(INotificationObserver o) {
	 observers.add(o);
	 for(INotificationObserver ob:observers) {
	 System.out.println(ob.getName());
	 }
	 }

	 @Override
	 public void removeSubscriber(INotificationObserver o) {
	 observers.remove(o);
	 for(INotificationObserver ob:observers) {
	 System.out.println(ob.getName());
	 }
	 }

	 @Override
	 public void notifySubscriber() {

	 for(INotificationObserver o:observers) {
	 o.onServerDown();
	 }
	 }

	 }