package ChainOfResponsiblity;

public class HR implements ILeaveRequestHandler{
	private ILeaveRequestHandler nextHandler = null;
	
	@Override
	public void HandleRequest(LeaveRequest leaveRequest)
	{
		int id = leaveRequest.getLeaveDays();
		
		if(id >= 7)
		{
			System.out.println(leaveRequest.getEmloyeeName() +  ", Please meet HR for your leave request");
		}
		else
		{
			System.out.println("Leave is approved by HR for :" + leaveRequest.getEmloyeeName());
		}
	}
}
