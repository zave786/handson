package ChainOfResponsiblity;


public class Program {
	public static void main(String[] args) {
		
		LeaveRequest leaveRequest = new LeaveRequest();
		
		leaveRequest.setEmployeeName("Rohan");
		leaveRequest.setLeaveDays(10);
		
		ILeaveRequestHandler supervior = new Supervisor();
		supervior.HandleRequest(leaveRequest);
		
		leaveRequest.setEmployeeName("Rakhi");
		leaveRequest.setLeaveDays(2);
		
		ILeaveRequestHandler supervior1 = new Supervisor();
		supervior1.HandleRequest(leaveRequest);
	}
}

