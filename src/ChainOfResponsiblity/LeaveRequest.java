package ChainOfResponsiblity;

public class LeaveRequest {

	private String employeeName;
	private int leaveDays;
	
	
	public String getEmloyeeName()
	{
		return employeeName;
	}
	
	public void setEmployeeName(String employeeName)
	{
		this.employeeName = employeeName;
	}
	
	public int getLeaveDays()
	{
		return leaveDays;
	}
	
	public void setLeaveDays(int leaveDays)
	{
		this.leaveDays = leaveDays;
	}
}
