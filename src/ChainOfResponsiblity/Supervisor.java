package ChainOfResponsiblity;


public class Supervisor implements ILeaveRequestHandler{

	private ILeaveRequestHandler nextHandler = new ProjectManager();

	@Override
	public void HandleRequest(LeaveRequest leaveRequest) {
		int id = leaveRequest.getLeaveDays();
		
		if(id > 0 && id < 3)
		{
			System.out.println("Leave is approaved for  : " + leaveRequest.getEmloyeeName());
		}
		else if(id >= 3)
		{
			nextHandler.HandleRequest(leaveRequest);
		}
	}

}

