package ChainOfResponsiblity;


public class ProjectManager implements ILeaveRequestHandler{

	private ILeaveRequestHandler nextHandler = new HR();

	@Override
	public void HandleRequest(LeaveRequest leaveRequest) {

		int id = leaveRequest.getLeaveDays();
		
		if(id < 5)
		{
			System.out.println("Leave is approaved for : " + leaveRequest.getEmloyeeName());
		}
		else
		{
			nextHandler.HandleRequest(leaveRequest);
		}
	}
}

