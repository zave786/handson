package Car;
enum CarType {

MICRO,MINI,LUXURY
}

 enum Location {
DEFAULT,USA,INDIA
}

 abstract class Car {

private CarType model = null;
private Location location = null;

public Car(CarType model, Location location) {
this.model = model;
this.location = location;
}

abstract void construct();

public CarType getModel() {
return model;
}

void setModel(CarType model) {
this.model = model;
}

Location getLocation() {
return location;
}

void setLocation(Location location) {
this.location = location;
}
@Override
public String toString()
{
return "CarModel - "+model + " located in "+location;
}

}

 class LuxuryCar extends Car {

LuxuryCar(Location location) {
super(CarType.LUXURY, location);
construct();
}

@Override
void construct() {
System.out.println("Connecting to luxury car");
}

}

 class MicroCar extends Car {

MicroCar(Location location)
{
super(CarType.MICRO, location);
construct();
}
@Override
void construct() {

System.out.println("Connecting to Micro Car ");
}

}

 class MiniCar extends Car {
MiniCar(Location location)
{
super(CarType.MINI,location );
construct();
}
@Override
void construct() {
// TODO Auto-generated method stub
System.out.println("Connecting to Mini car");
}

}

 class CarFactory {

public static Car buildCar(CarType c, Location l) {
Car car=null;
switch(l) {
case USA: car= USAFactory.buildCar(c);
return car;
case INDIA: car=IndiaFactory.buildCar(c);
return car;
case DEFAULT: car=DefaultFactory.buildCar(c);
return car;
}
return null;
}
}

 class DefaultFactory extends CarFactory {

public static Car buildCar(CarType c) {
switch(c) {
case MICRO: return new MicroCar(Location.DEFAULT);
case MINI: return new MiniCar(Location.DEFAULT);
case LUXURY: return new LuxuryCar(Location.DEFAULT);
}
return null;

}
}

 class IndiaFactory extends CarFactory {

public static Car buildCar(CarType c) {
switch(c) {
case MICRO: return new MicroCar(Location.INDIA);
case MINI: return new MiniCar(Location.INDIA);
case LUXURY: return new LuxuryCar(Location.INDIA);
}
return null;

}
}

 class USAFactory extends CarFactory {

public static Car buildCar(CarType c) {
switch(c) {
case MICRO: return new MicroCar(Location.USA);
case MINI: return new MiniCar(Location.USA);
case LUXURY: return new LuxuryCar(Location.USA);
}
return null;
}
}

public class CarClient {

public static void main(String[] args) {
System.out.println(CarFactory.buildCar(CarType.MICRO,Location.USA));
System.out.println(CarFactory.buildCar(CarType.MINI,Location.INDIA));
System.out.println(CarFactory.buildCar(CarType.LUXURY,Location.DEFAULT));

}

}