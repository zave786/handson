package Abstract;
public class FactoryCreator {
	
	public static AbstractFactory getFactory() {
		return new CarFactory();
	}

}
