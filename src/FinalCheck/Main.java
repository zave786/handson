package FinalCheck;

import java.util.ArrayList;
public class Main {

	public static void main(String[] args) {

		INotificationObserver admin1 = new AdminObserver("ABC");
		INotificationObserver admin2 = new AdminObserver("BCD");
		INotificationObserver admin3 = new AdminObserver("CDE"); 
		
		INotificationService service = new NotificationService();
		
		service.AddSubscriber(admin1);
		service.AddSubscriber(admin2);
		service.AddSubscriber(admin3);
		Event event1 = new Event("Coke Studio", 500);
		Event event2 = new Event("MTV Roadies", 80);
		Event event3 = new Event("Sunburn DJ", 101);
		
		service.NotifySubscriber(event1);
		
		service.RemoveSubscriber(admin2);
		
		service.NotifySubscriber(event2);
		
		service.NotifySubscriber(event3);
	}

}

 class AdminObserver extends INotificationObserver {

	public AdminObserver(String name) {
		this.setName(name);
	}
	@Override
	public void SendNotification(String eventName) {
		System.out.println(this.getName() + " : " + eventName + " is getting popular.");
	}
}

 class Event {
	private String name;
	private int ticketSold;
	
	public Event(String name, int ticketSold) {
		super();
		this.name = name;
		this.ticketSold = ticketSold;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getTicketSold() {
		return ticketSold;
	}

	public void setTicketSold(int ticketSold) {
		this.ticketSold = ticketSold;
	}

}

 abstract class INotificationObserver {
	private String name;

	public abstract void SendNotification(String eventName);

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "INotificationObserver [name=" + name + "]";
	}

}

 interface INotificationService {
	
	public void AddSubscriber(INotificationObserver observer);
	public void RemoveSubscriber(INotificationObserver observer);
	public void NotifySubscriber(Event event);
	
}


 class NotificationService implements INotificationService {

	private ArrayList<INotificationObserver> observers;

	public NotificationService() {
		observers = new ArrayList<>();
	}

	@Override
	public void AddSubscriber(INotificationObserver observer) {
		observers.add(observer);
		System.out.println("Observers: " + observers);
	}

	@Override
	public void RemoveSubscriber(INotificationObserver observer) {
		observers.remove(observer);
		System.out.println("Observers: " + observers);
	}

	@Override
	public void NotifySubscriber(Event event) {
		if (event.getTicketSold() >= 100) {
			for (INotificationObserver obs : observers) {
				obs.SendNotification(event.getName());
			}
		}
	}

}

