package FinalCheck;


public class Client {
	public static void main(String[] args) {
		AbstractFactory factory = FactoryCreator.getFactory();
		System.out.println(factory.createOrder("E-commerce Website", "Electronic"));

		System.out.println(factory.createOrder("E-commerce Website", "Furniture"));

		System.out.println(factory.createOrder("E-commerce Website", "Toy"));
		
		System.out.println(factory.createOrder("Tele caller Agent Application", "Electronic"));

		System.out.println(factory.createOrder("Tele caller Agent Application", "Furniture"));

		System.out.println(factory.createOrder("Tele caller Agent Application", "Toy"));
	}
}

 abstract class AbstractFactory {
	public abstract Order createOrder(String channel, String productType);
}


 class ElectronicOrder extends Order {

	public ElectronicOrder(String channel) {
		this.setChannel(channel);
		this.setProductType("Electronic");
		processOrder();
	}
	@Override
	public void processOrder() {
		System.out.println("Processing Electronic Order.");
	}
}


 class FactoryCreator {
	public static AbstractFactory getFactory() {
		return new OrderFactory();
	}
}


 class FurnitureOrder extends Order {

	public FurnitureOrder(String channel) {
		this.setChannel(channel);
		this.setProductType("Furniture");
		processOrder();
	}

	@Override
	public void processOrder() {
		System.out.println("Processing Furniture Order.");
	}
}


 abstract class Order {
	private String channel;
	private String productType;

	public abstract void processOrder();

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	@Override
	public String toString() {
		return "Order [channel=" + channel + ", productType=" + productType + "]";
	}

}


 class OrderFactory extends AbstractFactory {

	@Override
	public Order createOrder(String channel, String productType) {

		if (productType.equalsIgnoreCase("Electronic")) {
			return new ElectronicOrder(channel);
		} else if (productType.equalsIgnoreCase("Furniture")) {
			return new FurnitureOrder(channel);
		} else if (productType.equalsIgnoreCase("Toy")) {
			return new ToyOrder(channel);
		}

		return null;
	}

}


class ToyOrder extends Order {

	public ToyOrder(String channel) {
		this.setChannel(channel);
		this.setProductType("Toy");
		processOrder();
	}

	@Override
	public void processOrder() {
		System.out.println("Processing Toy Order.");
	}
}
